package com.maventest.maventest;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ichistruga on 9/21/2016.
 */
public class CalculatorTest {

    @Test
    public void add_test1() throws Exception {
        Calculator calc = new Calculator();
        calc.add(1,1);
        Assert.assertEquals(calc.getResult(), 2);
    }

    @Test
    public void add_test2() throws Exception {
        Calculator calc = new Calculator();
        calc.add(1,1);
        Assert.assertFalse(calc.getResult() == 0);
    }

    @Test
    public void multiply_test1() throws Exception {
        Calculator calc = new Calculator();
        calc.multiply(2,2);
        Assert.assertEquals(calc.getResult(), 4);
    }

    @Test(expected = UserException.class )
    public void multiply_test2() throws UserException{
        Calculator calc = new Calculator();
        calc.multiply();
    }

    @Test(expected = IllegalArgumentException.class )
    public void multiply_test3() throws UserException{
        Calculator calc = new Calculator();
        calc.multiply(1);
    }
}