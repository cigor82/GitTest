package com.maventest.maventest;

/**
 * Created by ichistruga on 9/21/2016.
 */
public class Cat extends Animal {

    public Cat(final String name){
        super(name);
    }

    public void catchMouse(){

    }

    @Override
    public void makeSound(){
        System.out.println(String.format("%s say: Myau", this.getName()));
    }
}
