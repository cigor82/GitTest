package com.maventest.maventest;

/**
 * Created by ichistruga on 9/21/2016.
 */
public class UserException extends Exception {

    public UserException(final String message){
        super(message);
    }
}
