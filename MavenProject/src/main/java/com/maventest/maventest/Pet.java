package com.maventest.maventest;

/**
 * Created by ichistruga on 9/21/2016.
 */
public interface Pet {
    void makeSound();

    String getName();
}
