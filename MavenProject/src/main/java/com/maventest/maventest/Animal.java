package com.maventest.maventest;
/**
 * Created by ichistruga on 9/21/2016.
 */
public class Animal{

    private final String name;

    public Animal(final String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
    public void makeSound()
    {
        System.out.println(String.format("%s say: beep", this.name));
    }
}