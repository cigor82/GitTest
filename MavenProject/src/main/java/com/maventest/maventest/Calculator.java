package com.maventest.maventest;

public class Calculator{
	private int result;
	
	public void add(int ... params){
		for(Integer i:params )
			this.result +=i;
	}

	/**
	 *
	 * @param params
	 * @throws UserException
	 */
	public void multiply(int ... params)throws UserException {
		if (params.length == 0)
			throw new UserException("Empty parameter list");
		if (params.length == 1)
			throw new IllegalArgumentException("There is need at least two parameter");

		result = result == 0 ? 1 : result;
		for (Integer i : params) {
			result *= i;
		}
	}
	
	public int getResult(){
		return this.result;
	}
	
	public void cleanResult(){
		 this.result = 0;
	}
	
	// public void add(int ... params){
		// for(Integer i:params )
			// this.result +=i
	// }
}