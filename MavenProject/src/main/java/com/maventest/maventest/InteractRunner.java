package com.maventest.maventest;

import java.util.*;

public class InteractRunner{
	
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		try{
			Calculator calc = new Calculator();
			calc.cleanResult();
			String exit = "no";
			while(!exit.equals("yes")){
				System.out.println("Enter first arg:");
				String first = reader.next();
				System.out.println("Enter second arg:");
				String second = reader.next();
				try {
					calc.multiply(); //Integer.valueOf(first), Integer.valueOf(second));
				} catch (UserException e) {
					System.out.println("Exception occurred:"+e.getMessage());
				}
				System.out.println("Value:"+calc.getResult());
				calc.cleanResult();
				System.out.println("Exit: yes/no:");
				exit  = reader.next();
			
			}
		}finally{
			reader.close();
		}
	}
}